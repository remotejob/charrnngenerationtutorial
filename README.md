# charrnngenerationtutorial
https://pytorch.org/tutorials/intermediate/char_rnn_generation_tutorial.html


kaggle datasets create -p data/
cd data; zip libs.zip libs/; cd ..

kaggle datasets version --dir-mode -p data/ -m "Updated data"


kaggle kernels push -p train/
kaggle kernels status sipvip/charrnngenerationtutorial
kaggle kernels output sipvip/charrnngenerationtutorial -p output/
cp output/model0.pt data/model0.pt
kaggle datasets version -p data/ -m "Updated data 0"
kaggle datasets status sipvip/char_rnn_classification_tutorialdata


kaggle kernels push -p generator/
kaggle kernels status sipvip/charrnngenerationtutorialgen
kaggle kernels output sipvip/charrnngenerationtutorialgen -p output/

