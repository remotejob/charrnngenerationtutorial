from __future__ import unicode_literals, print_function, division
from io import open
import os
import unicodedata
import string
import random
import time
import math


import torch
import torch.nn as nn
from torch.utils import data


class CustomDataset(data.Dataset):
    def __init__(self, path, transforms=None):
        self.lines = self.load_data(path)
        self.transforms = transforms

    def __getitem__(self, idx):

        line = self.lines[idx]
        return line

    def __len__(self):
        return len(self.lines)

    def all_letters(self):
        return string.ascii_letters + " .,;'-"

    def load_data(self, path):
        lines = open(path, encoding='utf-8').read().strip().split('\n')
        return [self.unicodeToAscii(line) for line in lines]

    def unicodeToAscii(self, s):
        return ''.join(
            c for c in unicodedata.normalize('NFD', s)
            if unicodedata.category(c) != 'Mn'
            and c in self.all_letters()
        )


use_cuda = torch.cuda.is_available()
device = torch.device('cuda' if use_cuda else 'cpu')
print(device)


class RNN(nn.Module):
    def __init__(self, input_size, hidden_size, output_size):
        super(RNN, self).__init__()
        self.hidden_size = hidden_size

        self.i2h = nn.Linear(input_size +
                             hidden_size, hidden_size)
        self.i2o = nn.Linear(input_size +
                             hidden_size, output_size)
        self.o2o = nn.Linear(hidden_size + output_size, output_size)
        self.dropout = nn.Dropout(0.1)
        self.softmax = nn.LogSoftmax(dim=1)

    def forward(self, input, hidden):
        input_combined = torch.cat((input, hidden), 1)
        input_combined = input_combined.to(device)
        hidden = self.i2h(input_combined)
        output = self.i2o(input_combined)
        output_combined = torch.cat((hidden, output), 1)
        output = self.o2o(output_combined)
        output = self.dropout(output)
        output = self.softmax(output)
        return output, hidden

    def initHidden(self):
        return torch.zeros(1, self.hidden_size)


def inputTensor(line):
    tensor = torch.zeros(len(line), 1, n_letters)
    for li in range(len(line)):
        letter = line[li]
        tensor[li][0][all_letters.find(letter)] = 1
    return tensor


def targetTensor(line):
    letter_indexes = [all_letters.find(line[li]) for li in range(1, len(line))]
    letter_indexes.append(n_letters - 1)  # EOS
    return torch.LongTensor(letter_indexes)


def train(input_line_tensor, target_line_tensor):

    input_line_tensor
    target_line_tensor
    target_line_tensor.unsqueeze_(-1)
    hidden = rnn.initHidden()
    hidden = hidden

    rnn.zero_grad()

    loss = 0

    for i in range(input_line_tensor.size(0)):
        output, hidden = rnn(
            input_line_tensor[i].to(device), hidden.to(device))
        l = criterion(output, target_line_tensor[i].to(device))
        loss += l

    loss.backward()

    for p in rnn.parameters():
        p.data.add_(-learning_rate, p.grad.data)

    return output, loss.item() / input_line_tensor.size(0)


def timeSince(since):
    now = time.time()
    s = now - since
    m = math.floor(s / 60)
    s -= m * 60
    return '%dm %ds' % (m, s)

print_every = 5000
epochs = 2
batchSize = 40


train_dataset = CustomDataset('../input/Finnish.txt')

train_loader = data.DataLoader(
    train_dataset, batch_size=batchSize, shuffle=True, drop_last=True)

all_letters = string.ascii_letters + " .,;'-"
n_letters = len(all_letters) + 1  # Plus EOS marker

criterion = nn.NLLLoss()

learning_rate = 0.0005

rnn = RNN(n_letters, 128, n_letters).to(device)

start = time.time()

for epoch in range(1, epochs + 1):

    for i, data in enumerate(train_loader, 0):

        for bi in range(batchSize):
            input_line_tensor = inputTensor(data[bi])
            target_line_tensor = targetTensor(data[bi])
            output, loss = train(input_line_tensor, target_line_tensor)

            if i % print_every == 0:
                print('%s %s %.4f' %
                      (timeSince(start), i, loss))


max_length = 15


def sample(start_letter='A'):
    with torch.no_grad():  # no need to track history in sampling

        input = inputTensor(start_letter)
        hidden = rnn.initHidden()

        output_name = start_letter

        for i in range(max_length):
            output, hidden = rnn(input[0].to(device), hidden.to(device))
            topv, topi = output.topk(1)
            topi = topi[0][0]
            if topi == n_letters - 1:
                break
            else:
                letter = all_letters[topi]
                output_name += letter
            input = inputTensor(letter)

        return output_name


def samples(start_letters='ABC'):
    for start_letter in start_letters:
        print(sample(start_letter))


samples('FIN')
